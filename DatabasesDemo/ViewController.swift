//
//  ViewController.swift
//  DatabasesDemo
//
//  Created by James Cash on 10-07-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        print("dogs: \(Database.shared.getAllDogs())")

        print("Dog #1: \(Database.shared.dogById(id: 1))")
        print("Dog #99: \(Database.shared.dogById(id: 99))")

        print("house dogs \(Database.shared.queryDog(query: "house"))")
        print("bark dogs \(Database.shared.queryDog(query: "bark"))")

        do {
            try Database.shared.insert(dog: Dog(id: 0, description: "Tiny dog running through the restaurant", location: "Sete Saias"))
        } catch let e {
            print("Error storing dog! \(e.localizedDescription)")
        }
        print("dogs: \(Database.shared.getAllDogs())")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

