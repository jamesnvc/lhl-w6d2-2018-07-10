//
//  Database.swift
//  DatabasesDemo
//
//  Created by James Cash on 10-07-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation
import FMDB

struct Dog {
    let id: Int32
    let description: String
    let location: String
}

class Database {
    private let db: FMDatabase

    // "singleton" pattern: this way to get an instance of this class, we just use Database.shared
    // what "singleton" means here is that there is only ever one instance of this class
    static let shared = Database()
    private init() {
        let docsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        // sometimes, when running in the simulator, this directory will change between builds
        // it really shouldn't, because it will break persistency
        print("Docs directory = \(docsURL)")

        db = FMDatabase(url: docsURL.appendingPathComponent("ourdata.sqlite"))
        if !db.open() {
            print("Couldn't open database")
            abort()
        }
    }

    deinit {
        db.close()
    }

    func createTables()  {
        do {
            try db.executeUpdate("""
CREATE TABLE IF NOT EXISTS funny_dogs (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   description text NOT NULL,
   rating integer,
   location text
);
""", values: nil)
        } catch let e {
            print("Error creating tables: \(e.localizedDescription)")
        }
    }

    func seedDatabase() {
        do {
            try db.executeUpdate("""
INSERT INTO funny_dogs (description, location, rating) VALUES
('Shitty dog that barks at us', 'drug dealers house', 5),
('Funny basset hound with head through bars', 'near the windmill', 10);
""", values: nil)
        } catch let e {
            print("Error seeding database: \(e.localizedDescription)")
        }
    }

    func getAllDogs() -> [[Int32:String]] {
        do {
            var dogs = [[Int32:String]]()
            let results = try db.executeQuery("select id, description from funny_dogs;", values: nil)
            while results.next() {
                let dogId = results.int(forColumn: "id")
                let dogDescription = results.string(forColumn: "description")!
                dogs.append([dogId: dogDescription])
            }
            return dogs
        } catch let e {
            print("Error querying all dogs: \(e.localizedDescription)")
            abort()
        }
    }

    func dogById(id: Int32) -> Dog? {
        // NEVER DO THIS
        //            "select * from funny_dogs where id = \(id)"
        // this leaves you open to SQL injection attacks in general, but is also just not good to do
        // e.g.
        /*
         let query = "bob'; drop database users; --"
         "select * from search_things where query = '\(query)';"
         */
        guard let results = db.executeQuery("select * from funny_dogs where id = :id;", withParameterDictionary: ["id": id]) else {
            print("Couldn't fetch results")
            return nil
        }

        while results.next() {
            return Dog(id: results.int(forColumn: "id"),
                       description: results.string(forColumn: "description")!,
                       location: results.string(forColumn: "location")!)
        }
        // if the result set is empty, return nil
        return nil
    }

    func queryDog(query: String) -> [Dog] {
        var dogs = [Dog]()
        guard let results = db.executeQuery("""
SELECT * FROM funny_dogs
WHERE description LIKE '%' || :query || '%'
OR location LIKE '%' || :query || '%';
""", withParameterDictionary: ["query": query])
            else {
                print("Couldn't search for dogs")
                return dogs
        }

        while results.next() {
            dogs.append(Dog(id: results.int(forColumn: "id"),
                            description: results.string(forColumn: "description")!,
                            location: results.string(forColumn: "location")!))
        }
        return dogs
    }

    func insert(dog: Dog) throws {
        db.executeUpdate("INSERT INTO funny_dogs (description, location) VALUES (:description, :location);",
                         withParameterDictionary: ["description": dog.description,
                                                   "location": dog.location])
    }
}
